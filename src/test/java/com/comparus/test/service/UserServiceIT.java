package com.comparus.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.comparus.test.ComparusDatabaseAggregatorApplication;
import com.comparus.test.config.DataSourceProperties;
import com.comparus.test.config.DataSources;
import com.comparus.test.model.SearchDataFilter;
import com.comparus.test.model.UserDto;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest(classes = ComparusDatabaseAggregatorApplication.class)
@Testcontainers
class UserServiceIT {

  private static final String TEST_USERNAME = "userName";

  @Autowired private UserService userService;
  @Autowired private DataSources dataSources;

  @Container
  private static final PostgreSQLContainer<?> postgresContainer1 =
      new PostgreSQLContainer<>("postgres:latest");

  @Container
  private static final PostgreSQLContainer<?> postgresContainer2 =
      new PostgreSQLContainer<>("postgres:latest");

  private SearchDataFilter filters;

  @BeforeEach
  void init() {
    postgresContainer1.start();
    postgresContainer2.start();
    filters = new SearchDataFilter();
  }

  @Test
  void testGetAllUsersWithoutFilters() {
    // GIVEN
    createTestDataSource(postgresContainer1, 0);
    createTestDataSource(postgresContainer2, 1);

    // WHEN
    List<UserDto> users = userService.getAllUsers(filters);

    // THEN
    users.forEach(
        user -> {
          assertNotNull(user.getId());
          assertNotNull(user.getName());
          assertNotNull(user.getUsername());
        });
  }

  @Test
  void testGetAllUsersWithFilters() {
    // GIVEN
    filters.setUsername(TEST_USERNAME);
    createTestDataSource(postgresContainer1, 0);
    createTestDataSource(postgresContainer2, 1);

    // WHEN
    List<UserDto> users = userService.getAllUsers(filters);

    // THEN
    users.forEach(
        user -> {
          assertNotNull(user.getId());
          assertNotNull(user.getName());
          assertNotNull(user.getUsername());
          assertEquals(TEST_USERNAME, user.getUsername());
        });
  }

  private void createTestDataSource(PostgreSQLContainer<?> container, int sourceNumber) {
    DataSourceProperties source = dataSources.getSources().get(sourceNumber);
    source.setUrl(container.getJdbcUrl());
    source.setUser(container.getUsername());
    source.setPassword(container.getPassword());
  }

  @AfterEach
  void closeUp() {
    postgresContainer1.stop();
    postgresContainer2.stop();
  }
}
