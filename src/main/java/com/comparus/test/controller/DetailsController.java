package com.comparus.test.controller;

import com.comparus.test.api.UsersApi;
import com.comparus.test.model.SearchDataFilter;
import com.comparus.test.model.UserDto;
import com.comparus.test.service.UserService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/** Controller for handling user details retrieval. */
@RestController
@RequiredArgsConstructor
public class DetailsController implements UsersApi {

  private final UserService userService;

  @Override
  public ResponseEntity<List<UserDto>> getUsers(SearchDataFilter filter) {
    List<UserDto> users = userService.getAllUsers(filter);
    return new ResponseEntity<>(users, HttpStatus.OK);
  }
}
