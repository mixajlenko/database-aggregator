package com.comparus.test.config;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_SINGLETON;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = SCOPE_SINGLETON)
public class DataSourceContextHolder {
  private static final ThreadLocal<String> threadLocal = new ThreadLocal<>();

  /** Sets the context for the current thread's DataSource. */
  public void setBranchContext(String dataSource) {
    threadLocal.set(dataSource);
  }

  /** Gets the context for the current thread's DataSource. */
  public String getBranchContext() {
    return threadLocal.get();
  }

  /** Clears the context for the current thread's DataSource. */
  public static void clearBranchContext() {
    threadLocal.remove();
  }
}
