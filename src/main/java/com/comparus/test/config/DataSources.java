package com.comparus.test.config;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Configuration properties for multiple DataSources. */
@Data
@Component
@ConfigurationProperties("application.datasource")
public class DataSources {

  /** List of DataSource properties. */
  private List<DataSourceProperties> sources;
}
