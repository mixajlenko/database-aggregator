package com.comparus.test.config;

import java.util.Map;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class DataSourceProperties {

  private String name;
  private String strategy;
  private String url;
  private String table;
  private String user;
  private String password;
  private Map<String, String> mapping;
}