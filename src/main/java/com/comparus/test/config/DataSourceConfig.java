package com.comparus.test.config;

import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;

@Configuration
@RequiredArgsConstructor
@DependsOn("dataSourceRouting")
public class DataSourceConfig {

  /** Primary DataSource bean definition. */
  private final DataSourceRouting dataSourceRouting;

  @Bean
  @Primary
  public DataSource dataSource() {
    return dataSourceRouting;
  }
}
