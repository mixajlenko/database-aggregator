package com.comparus.test.config;

import static java.util.stream.Collectors.toMap;

import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.stereotype.Component;

/** DataSourceRouting class for dynamic DataSource routing. */
@Component
public class DataSourceRouting extends AbstractRoutingDataSource {

  private final DataSources dataSources;
  private final DataSourceContextHolder dataSourceContextHolder;

  /** Constructs a new DataSourceRouting instance. */
  public DataSourceRouting(
      DataSourceContextHolder dataSourceContextHolder, DataSources dataSources) {
    this.dataSources = dataSources;
    this.dataSourceContextHolder = dataSourceContextHolder;

    Map<Object, Object> dataSourceMap = getExistsDataSources(dataSources);

    setTargetDataSources(dataSourceMap);
    setDefaultTargetDataSource(dataSourceMap.values().iterator().next());
  }

  /** Determines the current lookup key, i.e., the current DataSource. */
  @Override
  protected Object determineCurrentLookupKey() {
    return dataSourceContextHolder.getBranchContext();
  }

  private Map<Object, Object> getExistsDataSources(DataSources dataSources) {
    return dataSources.getSources().stream()
        .collect(
            toMap(
                DataSourceProperties::getName,
                sources -> dataSourceDataSource(sources.getName()),
                (a, b) -> b));
  }

  /** Creates a new DataSource based on the given key. */
  public DataSource dataSourceDataSource(String key) {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    DataSourceProperties dataSourceProperties =
        dataSources.getSources().stream()
            .filter(source -> source.getName().equals(key))
            .findFirst()
            .orElseThrow();
    dataSource.setUrl(dataSourceProperties.getUrl());
    dataSource.setUsername(dataSourceProperties.getUser());
    dataSource.setPassword(dataSourceProperties.getPassword());

    return dataSource;
  }
}
