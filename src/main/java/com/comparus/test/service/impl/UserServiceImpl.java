package com.comparus.test.service.impl;

import static com.comparus.test.config.DataSourceContextHolder.clearBranchContext;
import static com.comparus.test.util.DynamicDtoUtil.createDynamicDto;

import com.comparus.test.config.DataSourceContextHolder;
import com.comparus.test.config.DataSourceProperties;
import com.comparus.test.config.DataSources;
import com.comparus.test.model.SearchDataFilter;
import com.comparus.test.model.UserDto;
import com.comparus.test.repository.CustomUserRepository;
import com.comparus.test.service.UserService;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

  private final DataSourceContextHolder dataSourceContextHolder;
  private final DataSources dataSources;
  private final CustomUserRepository customUserRepository;

  @Override
  public List<UserDto> getAllUsers(SearchDataFilter filter) {
    List<UserDto> users = new ArrayList<>();
    dataSources
        .getSources()
        .forEach(
            source -> {
              List<Map<String, Object>> usersDataFromDatabase =
                  getDataFromDataSource(source, filter);
              usersDataFromDatabase.forEach(data -> getUsersBasedOnMapping(source, data, users));
            });
    return users;
  }

  private static void getUsersBasedOnMapping(
      DataSourceProperties source, Map<String, Object> data, List<UserDto> users) {
    try {
      Map<String, String> mapping = source.getMapping();
      Object dynamicDto = createDynamicDto(UserDto.class, mapping, data);
      users.add((UserDto) dynamicDto);
    } catch (InstantiationException
        | IllegalAccessException
        | NoSuchMethodException
        | InvocationTargetException e) {
      log.error("User dto not created due to an error", e);
    }
  }

  private List<Map<String, Object>> getDataFromDataSource(
      DataSourceProperties source, SearchDataFilter filter) {
    dataSourceContextHolder.setBranchContext(source.getName());
    List<Map<String, Object>> usersDataFromDatabase =
        customUserRepository.getUsersDataFromDatabase(source, filter);
    clearBranchContext();

    return usersDataFromDatabase;
  }
}
