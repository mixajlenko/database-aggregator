package com.comparus.test.service;

import com.comparus.test.model.SearchDataFilter;
import com.comparus.test.model.UserDto;
import java.util.List;

/** Service interface for user-related operations. */
public interface UserService {

  /**
   * Retrieves user data from all data sources and converts it into a list of User DTOs. Filters can
   * be used to determine specific data from the database.
   *
   * @param filter The filter criteria for user data retrieval.
   * @return A list of User DTOs containing user details.
   */
  List<UserDto> getAllUsers(SearchDataFilter filter);
}
