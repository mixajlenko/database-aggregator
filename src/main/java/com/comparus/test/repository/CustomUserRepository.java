package com.comparus.test.repository;

import static io.micrometer.common.util.StringUtils.isNotEmpty;
import static java.util.Objects.isNull;

import com.comparus.test.config.DataSourceProperties;
import com.comparus.test.model.SearchDataFilter;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.stereotype.Repository;

/** Repository class for handling user data retrieval from multiple DataSources. */
@Repository
@Transactional
@RequiredArgsConstructor
public class CustomUserRepository {

  private static final String AND_QUERY_PART = " AND ";
  private static final String EQUAL_QUERY_PART = " = '";
  private static final String QUOTE_QUERY_PART = "'";

  private final JdbcTemplate jdbcTemplate;

  /**
   * Retrieves user data from the specified data source based on the provided filter criteria.
   *
   * @param source The DataSourceProperties representing the data source.
   * @param filter The filter criteria for user data retrieval.
   * @return A list of maps containing user data.
   */
  public List<Map<String, Object>> getUsersDataFromDatabase(
      DataSourceProperties source, SearchDataFilter filter) {
    DataSource targetDataSource = getTargetDataSource(source);
    JdbcTemplate template = new JdbcTemplate(targetDataSource);

    return template.queryForList(generateSqlQueryBasedOnFilter(source, filter));
  }

  /**
   * Retrieves the target DataSource based on the provided DataSourceProperties.
   *
   * @param source The DataSourceProperties representing the data source.
   * @return The DataSource for the specified data source.
   * @throws IllegalStateException If the DataSource is not found for the specified name.
   */
  private DataSource getTargetDataSource(DataSourceProperties source) {
    AbstractRoutingDataSource routingDataSource =
        (AbstractRoutingDataSource) jdbcTemplate.getDataSource();
    DataSource targetDataSource = routingDataSource.getResolvedDataSources().get(source.getName());
    if (isNull(targetDataSource)) {
      throw new IllegalStateException("DataSource not found for name: " + source.getName());
    }

    return targetDataSource;
  }

  /**
   * Generates a SQL query based on the provided filter criteria and DataSourceProperties.
   *
   * @param source The DataSourceProperties representing the data source.
   * @param filter The filter criteria for user data retrieval.
   * @return The generated SQL query as a String.
   */
  public String generateSqlQueryBasedOnFilter(
      DataSourceProperties source, SearchDataFilter filter) {
    StringBuilder sqlQuery = new StringBuilder("SELECT * FROM " + source.getTable() + " WHERE 1=1");
    Map<String, String> mapping = source.getMapping();
    createQueryWithFilter(filter.getUsername(), mapping, "username", sqlQuery);
    createQueryWithFilter(filter.getName(), mapping, "name", sqlQuery);
    createQueryWithFilter(filter.getSurname(), mapping, "surname", sqlQuery);

    return sqlQuery.toString();
  }

  /**
   * Creates a filter condition for the given filter value and field name.
   *
   * @param filter The filter value.
   * @param mapping The mapping between DTO fields and data fields.
   * @param username The field name to filter on.
   * @param sqlQuery The StringBuilder containing the current SQL query.
   */
  private void createQueryWithFilter(
      String filter, Map<String, String> mapping, String username, StringBuilder sqlQuery) {
    if (isNotEmpty(filter)) {
      String columnName = getColumnName(mapping, username);
      appendFilter(sqlQuery, columnName, filter);
    }
  }

  /**
   * Appends a filter condition to the SQL query.
   *
   * @param sqlQuery The StringBuilder containing the current SQL query.
   * @param columnName The name of the column to filter on.
   * @param filter The filter value.
   */
  private static void appendFilter(StringBuilder sqlQuery, String columnName, String filter) {
    sqlQuery
        .append(AND_QUERY_PART)
        .append(columnName)
        .append(EQUAL_QUERY_PART)
        .append(filter)
        .append(QUOTE_QUERY_PART);
  }

  /**
   * Retrieves the column name based on the provided mapping and field name.
   *
   * @param mapping The mapping between DTO fields and data fields.
   * @param fieldName The name of the field.
   * @return The column name associated with the field.
   */
  private String getColumnName(Map<String, String> mapping, String fieldName) {
    return mapping.getOrDefault(fieldName, fieldName);
  }
}
