package com.comparus.test.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/** Utility class for creating dynamic DTOs. */
@Slf4j
public class DynamicDtoUtil {

  private DynamicDtoUtil() {}

  /**
   * Creates a dynamic DTO instance and populates its fields based on the provided mapping and data.
   *
   * @param dtoClass The class of the DTO to be created.
   * @param mapping The mapping between DTO fields and data fields.
   * @param data The data to populate the DTO with.
   * @return A dynamic DTO instance.
   * @throws IllegalAccessException If there is an issue accessing the fields during population.
   * @throws InstantiationException If there is an issue creating an instance of the DTO.
   * @throws NoSuchMethodException If the constructor for the DTO is not found.
   * @throws InvocationTargetException If there is an issue invoking the constructor.
   */
  public static Object createDynamicDto(
      Class<?> dtoClass, Map<String, String> mapping, Map<String, Object> data)
      throws IllegalAccessException,
          InstantiationException,
          NoSuchMethodException,
          InvocationTargetException {
    Object dynamicDto = dtoClass.getDeclaredConstructor().newInstance();
    Map<String, String> dtoFieldNames = getDtoFieldValues(mapping, data);
    for (Map.Entry<String, String> entry : dtoFieldNames.entrySet()) {
      convertToUserDtoField(dtoClass, entry, dynamicDto);
    }

    return dynamicDto;
  }

  /**
   * Retrieves field values from the data based on the mapping.
   *
   * @param mapping The mapping between DTO fields and data fields.
   * @param data The data to retrieve field values from.
   * @return A map of DTO field names and their corresponding values.
   */
  private static Map<String, String> getDtoFieldValues(
      Map<String, String> mapping, Map<String, Object> data) {
    Map<String, String> dtoFieldNames = new HashMap<>();

    mapping.forEach(
        (dtoFieldName, databaseFieldName) -> {
          if (!data.containsKey(databaseFieldName)) {
            return;
          }
          Object value = data.get(databaseFieldName);
          dtoFieldNames.put(dtoFieldName, value.toString());
        });

    return dtoFieldNames;
  }

  /**
   * Converts and sets a field value in the dynamic DTO.
   *
   * @param dtoClass The class of the DTO.
   * @param entry A map entry representing a DTO field name and its value.
   * @param dynamicDto The dynamic DTO instance.
   * @throws IllegalAccessException If there is an issue accessing the field during population.
   */
  private static void convertToUserDtoField(
      Class<?> dtoClass, Map.Entry<String, String> entry, Object dynamicDto)
      throws IllegalAccessException {
    String fieldName = entry.getKey();
    String value = entry.getValue();

    try {
      Field field = dtoClass.getDeclaredField(fieldName);
      field.setAccessible(true);
      field.set(dynamicDto, value);
      field.setAccessible(false);
    } catch (NoSuchFieldException e) {
      log.error("Such field doesn't exists: " + fieldName, e);
    }
  }
}
